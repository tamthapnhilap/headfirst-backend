# SSL

## OpenSSL

- https://www.digitalocean.com/community/tutorials/openssl-essentials-working-with-ssl-certificates-private-keys-and-csrs

- https://devcenter.heroku.com/articles/ssl-certificate-self

### Window

```
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out MyCertificat.crt -keyout MyKey.key
```

```
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out mycert.crt -keyout mykey.key
```