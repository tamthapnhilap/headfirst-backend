# Headfirst Backend

### Prerequisites

1.Git
2.SSH
3.HTTP/HTTPS
4.Terminal Usage
5.Data Structures & Algorithms
5.Character Encoding
6.Github

### Headfirst Programming Language

1. Ruby ( Main )
2. Java ( Main )
3. Scalar( Main )
4. Go ( Main )
5. NodeJS ( Freelancer )
6. PHP ( Freelancer )
7. C++ ( Freetime )
8. C (Arduno)

#### Headfirst Ruby

1. IDE

- Sublime Text 3 : git, gitgutter, ruby completions
- RubyMine